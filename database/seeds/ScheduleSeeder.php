<?php

use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schedule')->insert([
            'sleep_time' => '2020-08-22 22:22:22',
            'hours_slept' => '6.05',
            'android_id' => 'billfdADAJF',
        ]);
        DB::table('schedule')->insert([
            'sleep_time' => "2020-08-23 21:10:22",
            'hours_slept' => '6.05',
            'android_id' => 'lfdADAJFNDKLA',
        ]);
        DB::table('schedule')->insert([
            'sleep_time' => "2020-08-22 22:22:22",
            'hours_slept' => '5.05',
            'android_id' => 'lfdADAJFNDKLA',
        ]);
        DB::table('schedule')->insert([
            'sleep_time' => "2020-08-22 13:22:22",
            'hours_slept' => '6.05',
            'android_id' => 'lfdADAJFNDKLA',
        ]);
        DB::table('schedule')->insert([
            'sleep_time' => "2020-08-22 14:22:22",
            'hours_slept' => '4.05',
            'android_id' => 'lfdADAJFNDKLA',
        ]);
        DB::table('schedule')->insert([
            'sleep_time' => "2020-08-22 15:22:22",
            'hours_slept' => '8.05',
            'android_id' => 'lfdADAJFNDKLA',
        ]);
        DB::table('schedule')->insert([
            'sleep_time' => "2020-08-22 16:22:22",
            'hours_slept' => '12.05',
            'android_id' => 'lfdADAJFNDKLA',
        ]);
        DB::table('schedule')->insert([
            'sleep_time' => "2020-08-22 17:22:22",
            'hours_slept' => '10.05',
            'android_id' => 'lfdADAJFNDKLA',
        ]);

    }
}
