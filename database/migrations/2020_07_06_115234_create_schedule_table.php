<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule', function (Blueprint $table) {
            $table->id()->increments();
            $table->timestamps();
            $table->datetime('sleep_time')->nullable();
            $table->float('hours_slept');
            $table->string('android_id');
            $table->foreign('android_id')->references('android_id')->on('user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule');
        Schema::table('schedule', function (Blueprint $table){
            $table->dropForeign(['androidId']);
            $table->dropColumn(['androidId']);
        });
    }
}
