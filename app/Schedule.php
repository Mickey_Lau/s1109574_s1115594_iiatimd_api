<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedule';

    // public $timestamps = false;


    public function schedules(){
        return $this->belongsTo('App\User');
    }
    protected $fillable = [
        'sleep_time', 'hours_slept', 'android_id'
    ];


}
