<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Schedule;

class ScheduleController extends Controller
{
    public function show(){
        return DB::table('schedule')->get();
    }

    public function latest(){
        $schedule = Schedule::Where("android_id", '=', $android_id)->latest();
        return $schedule;
    }    

    public function userSchedule($android_id){
        $schedule = Schedule::Where("android_id", '=', $android_id)->orderBy('id', 'DESC')->take(7)->get()->reverse()->values();
        return $schedule;
    }

    public function create(Request $request){
        $schedule = new schedule([
            'hours_slept' => $request->hours_slept,
            'android_id' => $request->android_id,
            'sleep_time' => $request->sleep_time,
        ]);

        try{
            $schedule->save();

        }
        catch(Exception $e){
            return response()->json([
                'message' => 'create schedule failed'
            ]);

        }

        return response()->json([
            'message' => 'create schedule succ'
        ]);

      
    }

}
