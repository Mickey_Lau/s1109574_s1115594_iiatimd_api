<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'user';
    protected $fillable = [
      'android_id',
    ];
    public function schedules(){
        return $this->hasMany('App\Schedule');
    }

}
