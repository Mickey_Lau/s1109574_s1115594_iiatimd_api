<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('schedule/show', "ScheduleController@show");
Route::get('user/show', "UserController@show");
Route::post('user/add', "UserController@addUser");
Route::post('schedule/create', "ScheduleController@Create");
Route::get('{android_id}/show',"ScheduleController@userSchedule");
Route::get('{android_id}/latest',"ScheduleController@userSchedule");


